//Program to demonstrate paramaterized constructor and destructor

import java.util.*;

class Student {             //Main class
    String collegeName,fullName;
    double semPercentage;
    int collegeCode;
    Student(){          //Default constructor
        collegeName = "MVGR";
        collegeCode = 33;
    }
    void display1(){
        System.out.println("College Name " +collegeName);
        System.out.println("College Code " +collegeCode);
    } 
    Student(String Name, double Percentage){        //Parameterized Constructor
        fullName = Name;
        semPercentage = Percentage;
    }
    void display2(){
        System.out.println("Name " +fullName);
        System.out.println("Percentage " +semPercentage);
    }
    protected void finalize(){      //Destructor 
        System.out.println("Destructor got called");
    }
    public static void main(String[] args){
        System.out.println("Enter Name and sem percentage");
        Scanner sc = new Scanner (System.in);           //Object of Scanner class
        String name = sc.nextLine();                    //Taking string as input
        double p = sc.nextDouble();                     //Taking double type as input
        Student obj1 = new Student();                   //Object of default constructor
        Student obj2 = new Student(name,p);             //Object of parameterized consructor
        obj2.display2();
        obj1.display1();
    }
}
