//Program to demonstrate constructor and destructor

import java.util.*;

 class Student {        //Main class
        String fullName,collegeName;
        int rollNum,collegeCode;
        double semPercentage;
    Student(){      //Default constructor
        fullName="Sangeeta";
        rollNum= 3;
        semPercentage=90.6;
        collegeName = "MVGR";
        collegeCode = 33;
    }
    void display(){
        System.out.println("Name "+fullName);
        System.out.println("Rollno. "+rollNum);
        System.out.println("Percentage "+semPercentage);
        System.out.println("College Name "+collegeName);
        System.out.println("College Code "+collegeCode);
    } 
    protected void finalize(){      //Destructor in java
        System.out.println("Destructor got called");
    }
    public static void main(String[] args){
        System.out.println("Details");
        Student obj = new Student();        //object created for class
        obj.display();                      //Method call
    }
}
