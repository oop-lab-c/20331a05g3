//Program to demonstrate diamond problem using inheritance in java

class grandparent{                      //Base class
    grandparent(){
        System.out.println("Diamond Problem");
    }
}

class father extends grandparent {}     //Simple Inheritance

class mother extends grandparent {}     //Simple Inheritance

class child extends father,mother{}     //Multiple Inheritance - Not possible

public class MultipleInheriJava {
    public static void main(String[] args){
        child obj = new child();
    }
}
