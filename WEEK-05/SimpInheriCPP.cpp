//Program to demonstrate Simple Inheritance

#include<iostream>
using namespace std;

class common{       //Base class or parent class
    public:
    void subjects(){
        cout<<"PHYSICS, CHEMISTRY, ENGLISH, SANSKRIT "<<endl;
    }
};
class MPC : public common{      //Derived class or child class
    public:
    void extra(){
        cout<<"MATHS1A, MATHS1B "<<endl;
    }
};

int main(){
    MPC Obj;
    cout<<"MPC"<<endl;
    Obj.subjects();         //Calling parent method using child object
    Obj.extra();
}