//Program to demonstrate types of Inheritance

#include<iostream>
using namespace std;

class grandparent{  //Base class
    public:
    grandparent(){cout<<"Types of inheritance "<<endl;}
};
class stranger{};   //Empty base class
class parent : public grandparent {         //Single inheritance
    public:
    parent(){cout<<"Single inheritance"<<endl;}
};
class brother :  virtual public parent{     //Multilevel Inheritance
    public:
    brother(){cout<<"Multilevel Inheritance "<<endl;}
};
class sister : virtual public parent{       //Hierarchial Inheritance
    public:
    sister(){cout<<"Hierarchial Inheritance "<<endl;}
};
class cousin : virtual public brother , virtual public sister{      //Multiple inheritance
    public:
    cousin(){cout<<"Multiple Inheritance"<<endl;}
};
class grandchild : public cousin , public stranger{                 //Hybrid inheritance
    public:
    grandchild(){cout<<"Hybrid Inheritance "<<endl;}
};
int main(){
    grandchild g;
    return 0;
}