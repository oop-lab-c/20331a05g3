//Program to demonstrate the usage of Synchronization of threads 

class Table{
    synchronized void run(int variable){
         for(int i=1;i<5;i++){
             System.out.println(variable*i);
             try{
                 Thread.sleep(20);
            }catch(Exception e){
                 System.out.println(e);
            }
        }
    }
}
class FirstThread extends Thread{
    Table one;
    FirstThread(Table obj){
        one=obj;
    }
    public void run(){
        one.run(5);
    }
}
class SecondThread extends Thread{
    Table two;
    SecondThread(Table obj){
        two=obj;
    }
    public void run(){
        two.run(6);
    }
}

class ThreadSyncJava{
    public static void main(String[] args){
        Table obj3 = new Table();
        FirstThread obj1 = new FirstThread(obj3);
        SecondThread obj2 = new SecondThread(obj3);
        obj1.start();
        obj2.start();
    }
}
