//Program to compute arithmetic operations...

#include<iostream>
using namespace std;

int main()
{
    int a,b; //a and b variables of integer type
    char op; //op variable of character type
    cout<<"Enter the two integer values"<<endl;
    cin>>a>>b;
    cout<<"Enter the operator (+,-,*,/,%) "<<endl;
    cin>>op;
    switch(op) //Switch operator
    {
        case '+':
            cout<<"Sum is "<<a+b<<endl;
            break;
        case '-':
            cout<<"Difference is "<<a-b<<endl;
            break;
        case '*' :
            cout<<"Product is "<<a*b<<endl;
            break;
        case '/':
            if(b==0){
                cout<<"Division is not possible"<<endl;
                break;
            }else{
                cout<<"Quotient is "<<a/b<<endl;
                break;
            }
        case '%':
            cout<<"Remainder is "<<a%b<<endl;
            break;
        default:
            cout<<"Invalid operator"<<endl;
            break;
        return 0;
    }
}   