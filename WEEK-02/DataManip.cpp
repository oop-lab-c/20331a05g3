//Program to demonstrate data manipulators

#include<iostream>   //Header is part of input/output library
#include<iomanip>    //Header provides parametric manipulators
#include<string>     //Header provides string data type
#include<sstream>    //Header is part of input/output library
#include<istream>    //Header is part of input/output library( has ws manipulator)

using namespace std;

int main()
{
    int a=26 , b=15;
    float c=25.03935677;
    cout<<"Mvgr College"<<endl;                               //Inserts a new line
    cout<<setw(20)<<"Sangeeta Singh"<<endl;                   //Set width in output
    cout<<setfill('*')<<setw(20)<<"20331A05G3"<<endl;         //Fill character on output stream
    cout<<setprecision(4)<<c<<endl;                           //Set precision for floating point values
    istringstream str("      Hello World!!");
    string line;
    getline(str>>ws,line);                                   //Ignores whitespace in string sequence
    cout<<line<<endl;
    cout<<"All Done";
    cout<<flush<<endl;                                       //Synchronizes associated steam buffer sequence
}