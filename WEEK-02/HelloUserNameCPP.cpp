//Program to display Hello Username

#include<iostream>   //Including input output stream header files
using namespace std; // defining namespace

int main()  //Main function (must be of return type)
{
    string name;  //Declaring name variable of string type
    cout<<"Enter the User name"<<endl;
    cin>>name;  //Taking input of string type from user
    cout<<"Hello "<<name<<endl; //Printing Hello with User name
    return 0; //return value
}