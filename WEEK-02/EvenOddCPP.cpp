//Program to check whether given number is even or odd

#include<iostream>
using namespace std;

void even_odd(int num) //Function
{
    if(num%2==0) //Checking whether number is even or odd
    {
        cout<<"Number "<<num<<" is even"<<endl;
    }else{
        cout<<"Number "<<num<<" is odd"<<endl;
    }
}
int main(){
    int num;
    cout<<"Enter the number"<<endl;
    cin>>num;
    even_odd(num); //Function call
    return 0;
}