//Program to compute arithmetic operations

import java.util.*;

public class CalcJava {
      public static void main(String args[])
      { 
       System.out.println("Enter any two floating numbers");
       Scanner input = new Scanner (System.in);
       float a = input.nextFloat(); //Taking float input
       float b = input.nextFloat(); //Taking float input
       System.out.println("Enter operator of your choice (+,-,*,/,%)");
       char op = input.next().charAt(0); //Taking character as input
       if(op=='+'){
         System.out.println("The sum is "+ (a+b));
       }else if(op=='-'){
         System.out.println("The differnce is "+ (a-b));
       }else if(op=='*'){
         System.out.println("The product is "+ a*b);
       }else if(op=='/'){
         if(b==0)
          System.out.println("Division not possible");
        else
          System.out.println("The quotient is "+ a/b);
       }else if(op=='%'){
         System.out.println("The remainder is "+ a%b);
       }
      }
    }
