//Program to print the size of the integer data type in windows

#include<stdio.h> //Including standard header file

void main() //Main function
{
    printf("The size of int is %d \n", sizeof(int));//Printing size by using sizeof keyword
}