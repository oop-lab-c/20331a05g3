//Program to check whether given number is even or odd

import java.util.*;

class Check
 {
   void even(int num){
     if(num%2==0)  //Checking whether num is even
      System.out.println("The "+num+" is Even number");
   else
      System.out.println("The "+num+" is Odd number");
   }
}

public class EvenOddJava //Main class
 {
   public static void main(String args[])
   { 
    System.out.println("Enter a number");
    Scanner input = new Scanner(System.in);
    int num = input.nextInt(); //Taking Number to be checked
    Check obj = new Check();   //Creating object of a class
    obj.even(num);             //Method or Function Call
   }
 }



