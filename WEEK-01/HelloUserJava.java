//Program to print Hello UserName 

import java.util.*;  //Importing util package
class HelloUserJava  //Defining a class
{
    public static void main(String args[]) //Main block
    {
        System.out.println("Enter the User Name");
        Scanner input = new Scanner(System.in); //Creating object for scanner class
        String name = input.nextLine(); //Taking string as input from user
        System.out.println("Hello "+ name);  //Printing hello with username
    }
}