//Program to demonstrate method overloading in java using inheritance

class parent {
    void add(String a , String b){          //function with string arguments
        System.out.println(a+b);
    }
}
class child extends parent{
    void add(int a , int b){                  //function with integer arguments
        System.out.println(a+b);
    }
}
class MethodOLInheriJava{
    public static void main(String [] args){
        child obj = new child();
        obj.add("Water","melon");           //Overloads the function of child
        obj.add(6,7);                       //Overloads the function of parent
    }
}