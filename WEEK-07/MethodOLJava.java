//Program to demonstrate overloading in java

class overload {
    void print(int num){            //Function with argument of int type
        System.out.println("Number "+ num);
    }
    void print(String name){        //Function with argument of string type
        System.out.println("Name "+ name);
    }
}

public class MethodOLJava {
    public static void main(String[] args) {
        overload obj = new overload();
        obj.print(26);               //Overloads function which has string as argument
        obj.print("Harshita");      //Overloads function which has integer  as argument
    }
}
