//Program to demonstrate paetial abstraction in java using abstract classes

abstract class Boss{
    void deadline(){                    //Non Abstract method
        System.out.println("Time Limit : 10 Weeks");
    }
    abstract void projectchoice();       //Abstract method
}

class employee extends Boss{
    void projectchoice(){                   //Defining the abstract method
        System.out.println("Project : Student Attendance by Face detection ");
    }
}
public class ParAbsJava {
    public static void main(String[] args){
        employee obj = new employee();
        obj.projectchoice();
        obj.deadline();
    }
}