//Program to demonstrate pure abstraction..
#include<iostream>
using namespace std;

class parent{
         virtual void profession()=0;   //pure virtual function
};
class son: public parent{
    public:
        void profession(){              //definition of virtual function in child           
            cout<<"Actor "<<endl;
        }
};
class daughter : public parent{
    public:
        void profession(){              //definition of virtual function in another child
            cout<<"Pilot "<<endl;
        }
};
int main(){
    son s;
    daughter d;
    cout<<"Son profession"<<endl;
    s.profession();                     //calling virtual fuction defined in son class
    cout<<"Daughter profession"<<endl;
    d.profession();                     //calling virtual function defined in daughter class
}