//Program to demonstrate template function and template class
#include <iostream>
using namespace std;

template <class T>
class Number {          //template class
    T num;
   public:
    Number(T n){
       num = n;
    }  
    T getNum() {
        return num;
    }
};

template <typename S>           //template function
S add(S a, S b){
    return a+b;
}
int main() {

    Number<char> Obj(97);
    Number<int> Obj2(97);
    cout << "char Number = " << Obj.getNum() << endl;       //obj of ichar type
    cout << "int Number = " << Obj2.getNum() << endl;       //obj of int type
    cout << "float addition = " << add < float > (1.6 , 8.2) << endl;           //function of float type
    cout << "int addition = " << add < int > (6 , 8) << endl;                   //function of int type
    return 0;
}