//Program to demonstrate friend , inline functions

#include<iostream>
using namespace std;

inline void displayWelcomeMessage() //Inline function
{
    cout<<"Welcome"<<endl;
}
class Box
{
    private:
        float len,wid,hei;
    public:
        void setBoxDimensions(float l, float w, float h){
            len=l;
            wid=w;
            hei=h;
        }
        void boxArea(float l, float w) //Member function with definition
        {
            cout<<"Area of a box "<<l*w<<endl;
        }
        void boxVolume(float l, float w, float h);  //Member function with only declaration
        friend void displayBoxDimensions(Box obj);  //Friend function
};
void Box :: boxVolume(float l, float w, float h)   //Definition outside of class
{
    cout<<"Volume of a box "<<l*w*h<<endl;
}
void displayBoxDimensions(Box obj){
    cout<<"Length of a box "<<obj.len<<endl;
    cout<<"Width of a box "<<obj.wid<<endl;
    cout<<"Height of a box "<<obj.hei<<endl;
}
int main()
{
    Box obj;
    displayWelcomeMessage();
    float length,width,height;
    cout<<"Enter dimensions of box"<<endl;
    cin>>length>>width>>height;
    obj.setBoxDimensions(length,width,height);
    displayBoxDimensions(obj);
    obj.boxArea(length,width);
    obj.boxVolume(length,width,height);
}