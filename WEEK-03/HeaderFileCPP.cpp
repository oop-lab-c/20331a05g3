//Program to display Area and Volume of box using #ifdef an #ifndef
#include"boxArea.h"         //Including header files
#include"boxVolume.h"
using namespace std;

float len,wid,hei;      //Gloabal declaration

int main()
{
    #ifdef len      //Checking whether len,wid are defined 
    #ifdef wid 
        #undef len      //if defined we are undefining so that input could be taken from  user
        #undef wid 
    #endif
    #endif 
    #ifndef len     //Checking whether len,wid,hei is not defined
    #ifndef wid
    #ifndef hei
        cout<<"Enter the length, width, height of box"<<endl;
        cin>>len>>wid>>hei;         //Taking input from user as directed
        boxArea(len,wid);           //Function call  from header file
        boxVolume(len,wid,hei);
    #endif 
    #endif
    #endif
}
