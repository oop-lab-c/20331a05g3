//Program to demonstrate parameterized constructor and desstructor

#include<iostream>
using namespace std;
class Student
{
    public:
        string collegeName,fullName;
        double semPercentage;
        int collegeCode;
    Student(){              //default constructor
        collegeName= "MVGR";
        collegeCode= 33;
    }
    Student(string name,double percentage){             //parameterized constructor
        fullName=name;
        semPercentage=percentage;
    }
    void display1(){
        cout<<"College Name "<<collegeName<<endl;
        cout<<"College Code "<<collegeCode<<endl;
    }
    void display2(){
        cout<<"Full Name "<<fullName<<endl;
        cout<<"Percentage "<<semPercentage<<endl;
    }
    ~Student(){                 //destructor
        cout<<"The previous constructors got destructed"<<endl;
        exit(0);
    };
};
int main()
{
    string name;
    double p;
    cout<<"Enter name and percentage of student ";
    cin>>name;
    cin>>p;
    Student obj1;           //object of default constructor
    Student obj2(name,p);   //object of parametrized constructor
    obj2.display2();
    obj1.display1();
}
