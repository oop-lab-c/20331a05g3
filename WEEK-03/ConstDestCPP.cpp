//Program to demonstrate constructor and destructor

#include<iostream>
using namespace std;
class student
{
    public:
        string fullName,collegeName;
        int rollNum,collegeCode;
        double semPercentage;
    
    student()   //Constructor with no parameters
    {
        fullName="Sangeeta";
        rollNum=3;
        semPercentage=90.6;
        collegeName="MVGR";
        collegeCode=33;
    }
    void display(){
        cout<<"Name "<<fullName<<endl;
        cout<<"Rollno. "<<rollNum<<endl;
        cout<<"Percentage "<<semPercentage<<endl;
        cout<<"CollegeName "<<collegeName<<endl;
        cout<<"Collegecode "<<collegeCode<<endl;
    }
    ~student()  //Destructor
    {
        cout<<"Constructor got destructed"<<endl;
    };
    
};
int main()
{
    student obj;        //Creating object of class
    obj.display();      //Method call
}