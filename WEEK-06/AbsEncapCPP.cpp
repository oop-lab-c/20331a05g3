//Program to demonstrate Encpasulation 

#include<iostream>
using namespace std;
 
class AccessSpecifierDemo{
    //Member Variables
    private:
        int priVar;
    protected:
        int proVar;
    public:
        int pubVar;
    //Member Methods
    void setVar(int priValue,int proValue,int pubValue){
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    void getVar(){
        cout<<"Private value "<<priVar<<endl;
        cout<<"Protected value "<<proVar<<endl;
        cout<<"Public value "<<pubVar<<endl;
    }
};
int main(){
    cout<<"Enter the values of private,protected,public variables"<<endl;
    int pvt,pub,pro;
    cin>>pvt>>pro>>pub;             //Taking inputs
    AccessSpecifierDemo obj;        //Creating object
    obj.setVar(pvt,pro,pub);        //Assigning Values
    obj.getVar();                   //Displaying Values
}