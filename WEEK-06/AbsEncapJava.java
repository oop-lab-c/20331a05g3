//Program to demonstrate Encapsulation

import java.util.*;
public class AbsEncapJava {
    private int priVar;
    protected int proVar;
    public int pubVar;
    void setVar(int priValue,int proValue,int pubValue){
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    void getVar(){
        System.out.println("Private value " +  priVar);
        System.out.println("Protected value " +proVar);
        System.out.println("Public value " +pubVar);
    }
    public static void main(String[] args) {
        System.out.println("Enter private protected and public values");
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        AbsEncapJava obj = new AbsEncapJava();
        obj.setVar(a,b,c);
        obj.getVar();
    }
}
